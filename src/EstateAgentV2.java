public class EstateAgentV2 {
    public static void main(String[] args) {

        Home home2 = HomeBuilder2.startNormalBUilder("Azerbaijan", "Tovuz", "Cesmeli", 2013,5)
                .setDublex(true)
                .setHasCarPark(true)
                .build();


        Home home3 = HomeBuilder2.startWithPoolBUilder("Azerbaijan", "Shaki", "Shin", 1993,3)
                .setDublex(true)
                .setHasCarPark(true)
                .build();


        printHome(home2);
        printHome(home3);

    }

    private static void printHome(Home home) {
        System.out.println();
        System.out.println("Home added -> " + home);
        System.out.println();
    }

}

