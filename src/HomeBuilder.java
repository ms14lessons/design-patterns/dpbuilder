public class HomeBuilder {

    private String country;

    private String city;
    private String district;

    private int buildingYear;
    private int roomCount;
    private int bathroomCount;
    private int toiletCount;
    private int balconyCount;
    private boolean isDublex;
    private boolean isFurnished;

    private boolean hasCarPark;
    private boolean hasChildPark;

    private boolean hasAirConditioning;

    private boolean hasPool;


    public static HomeBuilder startBUilder() {
        return new HomeBuilder();
    }

    public Home build() {
        Home home = new Home();

        home.setCountry(country);
        home.setCity(city);
        home.setDistrict(district);
        home.setBuildingYear(buildingYear);
        home.setRoomCount(roomCount);
        home.setBathroomCount(bathroomCount);
        home.setToiletCount(toiletCount);
        home.setBalconyCount(balconyCount);
        home.setDublex(isDublex);
        home.setFurnished(isFurnished);
        home.setHasCarPark(hasCarPark);
        home.setHasChildPark(hasChildPark);
        home.setHasAirConditioning(hasAirConditioning);
        home.setHasPool(hasPool);
        return home;

    }

    public HomeBuilder setCountry(String country) {
        this.country = country;
        return this;
    }

    public HomeBuilder setCity(String city) {
        this.city = city;
        return this;
    }

    public HomeBuilder setDistrict(String district) {
        this.district = district;
        return this;
    }

    public HomeBuilder setBuildingYear(int buildingYear) {
        this.buildingYear = buildingYear;
        return this;
    }

    public HomeBuilder setRoomCount(int roomCount) {
        this.roomCount = roomCount;
        return this;
    }

    public HomeBuilder setBathroomCount(int bathroomCount) {
        this.bathroomCount = bathroomCount;
        return this;
    }

    public HomeBuilder setToiletCount(int toiletCount) {
        this.toiletCount = toiletCount;
        return this;
    }

    public HomeBuilder setBalconyCount(int balconyCount) {
        this.balconyCount = balconyCount;
        return this;
    }

    public HomeBuilder setDublex(boolean dublex) {
        isDublex = dublex;
        return this;
    }

    public HomeBuilder setFurnished(boolean furnished) {
        isFurnished = furnished;
        return this;
    }

    public HomeBuilder setHasCarPark(boolean hasCarPark) {
        this.hasCarPark = hasCarPark;
        return this;
    }

    public HomeBuilder setHasChildPark(boolean hasChildPark) {
        this.hasChildPark = hasChildPark;
        return this;
    }

    public HomeBuilder setHasAirConditioning(boolean hasAirConditioning) {
        this.hasAirConditioning = hasAirConditioning;
        return this;
    }

    public HomeBuilder setHasPool(boolean hasPool) {
        this.hasPool = hasPool;
        return this;
    }
}
