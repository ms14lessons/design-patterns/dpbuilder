public class EstateAgent {
    public static void main(String[] args) {

        Home home1 = new Home();

        home1.setCountry("Azerbaijan");
        home1.setCity("Baku");
        home1.setDistrict("Binagady");
        home1.setBalconyCount(2);
        home1.setRoomCount(3);
        home1.isHasAirConditioning(true);

        Home home2 = new Home("Azerbaijan", "Baku", "Yasamal", 1990, 4, 1, 1, 2, false, true, true, false, true, false);
        printHome(home1);
        printHome(home2);

    }

    private static void printHome(Home home) {
        System.out.println();
        System.out.println("Home added -> " + home);
        System.out.println();
    }

}

