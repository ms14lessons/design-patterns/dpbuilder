public class HomeBuilder2 {

    private String country;

    private String city;
    private String district;

    private int buildingYear;
    private int roomCount;
    private int bathroomCount;
    private int toiletCount;
    private int balconyCount;
    private boolean isDublex;
    private boolean isFurnished;

    private boolean hasCarPark;
    private boolean hasChildPark;

    private boolean hasAirConditioning;

    private boolean hasPool;


    public static HomeBuilder2 startNormalBUilder(String country, String city, String district, int buildingYear, int roomCount) {
        HomeBuilder2 homeBuilder2 = new HomeBuilder2();
        homeBuilder2.country = country;
        homeBuilder2.city = city;
        homeBuilder2.district = district;
        homeBuilder2.buildingYear = buildingYear;
        homeBuilder2.roomCount = roomCount;
        return homeBuilder2;
    }

    public static HomeBuilder2 startWithPoolBUilder(String country, String city, String district, int buildingYear, int roomCount) {
        HomeBuilder2 homeBuilder2 = new HomeBuilder2();
        homeBuilder2.country = country;
        homeBuilder2.city = city;
        homeBuilder2.district = district;
        homeBuilder2.buildingYear = buildingYear;
        homeBuilder2.roomCount = roomCount;
        homeBuilder2.hasPool=true;
        return homeBuilder2;
    }
    public Home build() {
        Home home = new Home();

        home.setCountry(country);
        home.setCity(city);
        home.setDistrict(district);
        home.setBuildingYear(buildingYear);
        home.setRoomCount(roomCount);
        home.setBathroomCount(bathroomCount);
        home.setToiletCount(toiletCount);
        home.setBalconyCount(balconyCount);
        home.setDublex(isDublex);
        home.setFurnished(isFurnished);
        home.setHasCarPark(hasCarPark);
        home.setHasChildPark(hasChildPark);
        home.setHasAirConditioning(hasAirConditioning);
        home.setHasPool(hasPool);
        return home;

    }

    public HomeBuilder2 setBathroomCount(int bathroomCount) {
        this.bathroomCount = bathroomCount;
        return this;
    }

    public HomeBuilder2 setToiletCount(int toiletCount) {
        this.toiletCount = toiletCount;
        return this;
    }

    public HomeBuilder2 setBalconyCount(int balconyCount) {
        this.balconyCount = balconyCount;
        return this;
    }

    public HomeBuilder2 setDublex(boolean dublex) {
        isDublex = dublex;
        return this;
    }

    public HomeBuilder2 setFurnished(boolean furnished) {
        isFurnished = furnished;
        return this;
    }

    public HomeBuilder2 setHasCarPark(boolean hasCarPark) {
        this.hasCarPark = hasCarPark;
        return this;
    }

    public HomeBuilder2 setHasChildPark(boolean hasChildPark) {
        this.hasChildPark = hasChildPark;
        return this;
    }

    public HomeBuilder2 setHasAirConditioning(boolean hasAirConditioning) {
        this.hasAirConditioning = hasAirConditioning;
        return this;
    }

    public HomeBuilder2 setHasPool(boolean hasPool) {
        this.hasPool = hasPool;
        return this;
    }
}
