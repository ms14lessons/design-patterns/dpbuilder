public class Home {
    private String country;

    private String city;
    private String district;

    private int buildingYear;
    private int roomCount;
    private int bathroomCount;
    private int toiletCount;
    private int balconyCount;
    private boolean isDublex;
    private boolean isFurnished;

    private boolean hasCarPark;
    private boolean hasChildPark;

    private boolean hasAirConditioning;

    private boolean hasPool;

    public Home() {
    }



    public Home(String country, String city, String district, int buildingYear, int roomCount, int bathroomCount, int toiletCount, int balconyCount, boolean isDublex, boolean isFurnished, boolean hasCarPark, boolean hasChildPark, boolean hasAirConditioning, boolean hasPool) {
        this.country = country;
        this.city = city;
        this.district = district;
        this.buildingYear = buildingYear;
        this.roomCount = roomCount;
        this.bathroomCount = bathroomCount;
        this.toiletCount = toiletCount;
        this.balconyCount = balconyCount;
        this.isDublex = isDublex;
        this.isFurnished = isFurnished;
        this.hasCarPark = hasCarPark;
        this.hasChildPark = hasChildPark;
        this.hasAirConditioning = hasAirConditioning;
        this.hasPool = hasPool;
    }

    public Home(String country, String city, String district, int buildingYear, int roomCount) {
        this.country = country;
        this.city = city;
        this.district = district;
        this.buildingYear = buildingYear;
        this.roomCount = roomCount;
        this.balconyCount = balconyCount;
    }
/*
    public Home(String country, String city, String district, int buildingYear, int roomCount, int bathroomCount) {
        this.country = country;
        this.city = city;
        this.district = district;
        this.buildingYear = buildingYear;
        this.roomCount = roomCount;
        this.bathroomCount = bathroomCount;
    }
*/

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getBuildingYear() {
        return buildingYear;
    }

    @Override
    public String toString() {
        return "Home{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", buildingYear=" + buildingYear +
                ", roomCount=" + roomCount +
                ", bathroomCount=" + bathroomCount +
                ", toiletCount=" + toiletCount +
                ", balconyCount=" + balconyCount +
                ", isDublex=" + isDublex +
                ", isFurnished=" + isFurnished +
                ", hasCarPark=" + hasCarPark +
                ", hasChildPark=" + hasChildPark +
                ", hasAirConditioning=" + hasAirConditioning +
                ", hasPool=" + hasPool +
                '}';
    }

    public void setBuildingYear(int buildingYear) {
        this.buildingYear = buildingYear;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public int getBathroomCount() {
        return bathroomCount;
    }

    public void setBathroomCount(int bathroomCount) {
        this.bathroomCount = bathroomCount;
    }

    public int getToiletCount() {
        return toiletCount;
    }

    public void setToiletCount(int toiletCount) {
        this.toiletCount = toiletCount;
    }

    public int getBalconyCount() {
        return balconyCount;
    }

    public void setBalconyCount(int balconyCount) {
        this.balconyCount = balconyCount;
    }

    public boolean isDublex() {
        return isDublex;
    }

    public void setDublex(boolean dublex) {
        isDublex = dublex;
    }

    public boolean isFurnished() {
        return isFurnished;
    }

    public void setFurnished(boolean furnished) {
        isFurnished = furnished;
    }

    public boolean isHasCarPark() {
        return hasCarPark;
    }

    public void setHasCarPark(boolean hasCarPark) {
        this.hasCarPark = hasCarPark;
    }

    public boolean isHasChildPark() {
        return hasChildPark;
    }

    public void setHasChildPark(boolean hasChildPark) {
        this.hasChildPark = hasChildPark;
    }

    public boolean isHasAirConditioning(boolean b) {
        return hasAirConditioning;
    }

    public void setHasAirConditioning(boolean hasAirConditioning) {
        this.hasAirConditioning = hasAirConditioning;
    }

    public boolean isHasPool() {
        return hasPool;
    }

    public void setHasPool(boolean hasPool) {
        this.hasPool = hasPool;
    }
}
